import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SEO_CONFIG, SeoConfig } from './seo-config';
import { SeoService } from './seo.service';

@NgModule({
  imports: [RouterModule],
  declarations: [],
  exports: []
})
export class SeoModule {

  static forRoot(seoConfig: SeoConfig): ModuleWithProviders {
    return {
      ngModule: SeoModule,
      providers: [
        { provide: SEO_CONFIG, useValue: seoConfig },
        SeoService
      ]
    };
  }
}
