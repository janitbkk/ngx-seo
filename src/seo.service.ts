import { Injectable, Inject } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { filter, map } from 'rxjs/operators';
import { SeoConfig, SEOImage, SEO_CONFIG } from './seo-config';

const DATA_PROPERTY = 'seo';
const DEFAULT_CONFIG: SeoConfig = {
  baseUrl: '',
  titlePrefix: '',
  titleSuffix: ''
};

@Injectable()
export class SeoService {
  private config: SeoConfig;

  constructor(
    @Inject(SEO_CONFIG) private seoConfig: SeoConfig,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private title: Title,
    private meta: Meta
  ) {
    this.config = { ...DEFAULT_CONFIG, ...seoConfig };
    this.router.events.pipe(
      filter(evt => evt instanceof NavigationEnd),
      map(_ => this.activatedRoute),
      map((route: ActivatedRoute) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      })
    )
    .subscribe(route => this.routeChange(route.snapshot));
  }

  routeChange(node) {
    if (node.title) this.updateTitle(node.title);
    if (node.description) this.updateDescription(node.description);
    if (node.img) this.updateImg(node.img);
    if (node.type) this.updateType(node.type);
    if (node.url) this.updateUrl(node.url);
    if (node.locale) this.updateLocale(node.locale);
    if (node.tags) this.updateTags(node.tags);
  }

  setTitle(title: string) {
    const NewTitle = this.config.titlePrefix + title + this.config.titleSuffix;
    this.title.setTitle(NewTitle);
    this.meta.updateTag(this.createOgTag('title', NewTitle));
  }

  updateDescription(desc: string) {
    this.meta.updateTag({ name: 'description', content: desc });
    this.meta.updateTag(this.createOgTag('description', desc));
  }

  updateImg(img: SEOImage) {
    if (img.url) this.meta.updateTag(this.createOgTag('image', img.url, 'url'));
    if (img.width) this.meta.updateTag(this.createOgTag('image', img.width.toString(), 'width'));
    if (img.height) this.meta.updateTag(this.createOgTag('image', img.height.toString(), 'height'));
    if (img.type) this.meta.updateTag(this.createOgTag('image', img.type, 'type'));
    if (img.alt) this.meta.updateTag(this.createOgTag('image', img.alt, 'alt'));
  }

  updateType(type: string) {
    if (!type) { type = 'website'; }
    this.meta.updateTag(this.createOgTag('type', type));
  }

  updateLocale(locale: string) {
    if (!locale) { locale = 'en_US'; }
    this.meta.updateTag(this.createOgTag('locale', locale));
  }

  updateUrl(url: string) {
    this.meta.updateTag(this.createOgTag('url', url));
  }

  updateTags(tags: string[]) {
    tags.forEach(tag => {
      this.meta.removeTag('property="og:article:tag"');
      this.meta.addTag(this.createOgTag('article', tag, 'tag'));
    });
  }

  createOgTag(property: string, content: string, property2?: string) {
    return {
      property: property2 ? 'og:' + property + ':' + property2 : 'og:' + property,
      content: content
    };
  }

  getSeoData(routeSnapshot: ActivatedRouteSnapshot) {
    return routeSnapshot.data && routeSnapshot.data[DATA_PROPERTY]
      ? routeSnapshot.data[DATA_PROPERTY]
      : {};
  }
}
