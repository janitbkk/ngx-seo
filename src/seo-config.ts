import { InjectionToken } from '@angular/core';

export const SEO_CONFIG = new InjectionToken('SeoConfigToken');

export interface SeoConfig {
    baseUrl: string;
    titleSuffix?: string;
    titlePrefix?: string;
    defaults?: SEONode;
}

export interface SEONode {
    title?: string;
    description?: string;
    img?: SEOImage;
    type?: string;
    url?: string;
    locale?: string;
    facebookAppId?: string;
    tags?: string[];
}

export interface SEOImage {
    url?: string;
    alt?: string;
    type?: string;
    height?: number;
    width?: number;
}
